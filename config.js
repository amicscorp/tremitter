module.exports = {
    server: {
        port: 3040
    },
    datastore: {
        path: './data/',
        mask: '^.*\.txt$'
    },
    payment: [
        {
            code: 'BTC',
            weight: 0.1,
            min: 0.001,
            max: 2.000,
            precision: 0.003
        },
        {
            code: 'USD',
            weight: 0.6,
            min: 5.00,
            max: 200.00,
            precision: 0.50
        },
        {
            code: 'USD',
            weight: 0.01, // there are some big $$$ time from time
            min: 200.00,
            max: 600.00,
            precision: 10
        },
        {
            code: 'EUR',
            weight: 0.4,
            min: 5.00,
            max: 300.00,
            precision: 1
        },
        {
            code: 'RUR',
            weight: 0.5,
            min: 500,
            max: 32000,
            precision: 500
        }
    ],
    masking: {
        mask: '*',
        min: 4,
        max: 5,
        coverage: 0.70
    },
    sid: {
        fallback: 4 * 1000 * 1000,
        file: 'sid',
        maxIncrease: 4,
        commitEvery: 1000,
        stackSize: 4
    },
    delay: {
        loop: 500,
        emitting: {min: 300, max: 6000}
    },
    category: [
        {id: 0, weight: 10},
        {id: 1, weight: 20},
        {id: 2, weight: 30},
        {id: 3, weight: 40}
    ]
};