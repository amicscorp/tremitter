# TrEmitter

Transaction emitter based powered upon `socket.io` (websockets with ajax fallback and state control) and `chance` (randomizing).

## Installation

Install git and node.js on server. Additionally install `gulp` for service management.

```bash
$ apt-get install git nodejs tmux
$ npm install -g gulp
```

Checkout repo, `cd` to app dir and install deps:

```bash
$ npm install
```

## Run

Simply execute `gulp` from app directory. That's all.

```bash
$ gulp
```

Gulp task uses `nodemon` to keep app forever alive. To reload changes type `rs` in app terminal session. 
If need to run background mode use session mupliplexor like `tmux` or `screen`.


## Usage 

Service running on port `config.server.port` (default is `3040`) and emitting events following to rules described in `config.js`

### Datastore

Datastore (input data) presented as plain line-based files. Sequence ID (sid) stored in plain text file pointed in `config.sid.file`.

### `config.js`

| Field | Description |
| ---             | ---  |
| payment         | describes currencies and ranges |
| payment.code    | currency ISO code |
| payment.min     | minimum amount |
| payment.max     | maximum amount |
| payment.precision | amount step, reduction ratio |
| payment.weight  | how often this transaction happens, higher value is more frequently |
| masking         | configuration related to person detail covering | 
| sid             | sequence identifier to keep transaction numbers actual and growing |
| delay           | setup timings for looper and transaction emitter (in ms) |
| category        | payment categories with weights |

### Emittion

| Field       | Type   | Description |
| ---         | ---    | ---         |
| timestamp   | Number | Hammertime/UNIX timestamp in ms |
| uid         | Number | Transaction id (auto incremented) |
| currency    | String | ISO currency code |
| amount      | Float  | Transaction amount in currency |
| from        | String | Sender info (covered) |
| to          | String | Receiver info (covered) |
| category    | Number | Payment category (for further connection with description) |

## License

ISC

© 2016 amics.co