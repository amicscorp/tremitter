/**
 * TrEmitter – transaction emitter
 *
 * @author w@amics.co
 * @date June 2016
 */

var config = require('./config.js'),
    app = require('http').createServer(onServer),
    io = require('socket.io')(app),
    chance = require('chance')(),
    path = require('path'),
    fs = require('fs');

var tremit = {
    clients: 0,
    sid: 0,
    lines: [],
    lastEmit: 0,
    nextTick: 0,
    stack: [],

    init: function(){
        // read datastore
        this.lines = tremit.readFilestore(config.datastore);
        if (!this.lines.length){
            console.log('No data; exiting');
            return;
        }

        // precalc weights
        config.paymentWeights = config.payment.map(function(e){
            return e.weight;
        });
        config.categoryWeight = config.category.map(function(e){
            return e.weight;
        });

        tremit.nextTick = new Date().getTime() + config.delay.loop * 2;

        // load sid
        fs.readFile(config.sid.file, function(err, data){
            if (err || !data)
                tremit.sid = 0;
            else
                tremit.sid = parseInt(data);

            if (!tremit.sid || tremit.sid < config.sid.fallback)
                tremit.sid = config.sid.fallback;

            // start
            setInterval(tremit.planner, config.delay.loop);
        });
    },

    nextSid: function(){
        tremit.sid += chance.integer({min: 1, max: config.sid.maxIncrease});
        if (tremit.sid % config.sid.commitEvery == 0){
            // save sid
            fs.writeFile(config.sid.file, tremit.sid);
        }
        return tremit.sid;
    },

    getCryptonite: function(){
        var line = tremit.lines[chance.integer({min: 0, max: tremit.lines.length - 1})];
        var part = line.split('@');
        var prefix = part[0];
        if (prefix.length < config.masking.min){
            prefix = config.masking.mask.repeat(config.masking.min + 1);
        } else {
            prefix = prefix.split('');
            if (prefix.length > config.masking.max){
                var middle = prefix.length * 0.5;
                var half = Math.floor(config.masking.max * 0.5);
                prefix = prefix.slice(0, Math.floor(middle - half)).concat(
                    prefix.slice(Math.floor(middle + half, prefix.length - (middle + half)))
                );
            }
            var nchars = Math.floor(prefix.length * config.masking.coverage);
            var p = 0;
            while (nchars > 0){
                p = chance.integer({min: 0, max: prefix.length});
                if (prefix[p] != config.masking.mask){
                    prefix[p] = config.masking.mask;
                    nchars--;
                }
            }
            prefix = prefix.join('');
        }
        return prefix + '@' + part[1];
    },

    readFilestore: function(store){
        var root = store.path.replace('./', __dirname + '/');
        if (!fs.existsSync(root)){
            return [];
        }
        var regex = new RegExp(store.mask);
        var content = '';
        var files = fs.readdirSync(root);
        for (i in files){
            var filename = path.join(root, files[i]);
            if (regex.test(filename)){
                content += fs.readFileSync(filename);
            }
        }
        return content.split('\n').filter(function(e){
            if (e.length && e.indexOf('@') > 0)
                return e.trim();
        });
    },

    buildMessage: function(time){
        var payment = chance.weighted(config.payment, config.paymentWeights);
        var amount = Math.floor(chance.floating({min: payment.min, max: payment.max}) / payment.precision) * payment.precision;
        if (payment.precision < 1){
            amount = amount.toFixed(('' + payment.precision).length - 1);
        }
        return {
            timestamp: time,
            uid: tremit.nextSid(),
            currency: payment.code,
            amount: amount,
            from: tremit.getCryptonite(),
            to: tremit.getCryptonite(),
            category: (chance.weighted(config.category, config.categoryWeight)).id
        };
    },

    planner: function(){
        var now = new Date().getTime();
        if (now < tremit.nextTick){
            return;
        }
        var message = tremit.buildMessage(tremit.nextTick - 15 * 1000);
        while (tremit.stack.length > config.sid.stackSize - 1)
            tremit.stack.pop();
        tremit.stack.push(message);

        tremit.nextTick += chance.integer(config.delay.emitting);

        if (tremit.clients >= 0){
            io.emit('message', message);
        }
    },

    sendStack: function(socket){
        tremit.stack.map(function(message){
            socket.emit('message', message);
        });
    }
};

/**
 * Basic HTTP requests handler
 * @param req
 * @param res
 */
function onServer(req, res){
    var reqFile = '.' + req.url;
    if (reqFile == './')
        reqFile = './index.html';

    var contentType = 'text/html';
    switch (path.extname(reqFile)){
        case '.js':
            contentType = 'text/javascript';
            break;
        case '.css':
            contentType = 'text/css';
            break;
        case '.png':
            contentType = 'image/png';
            break;
        case '.jpg':
            contentType = 'image/jpg';
            break;
    }

    fs.readFile(reqFile, function(err, data){
        if (err){
            res.writeHead(500);
            res.end('NOPE');
        } else {
            res.writeHead(200, {'Content-Type': contentType});
            res.end(data, 'utf-8');
        }
    });
}

app.listen(config.server.port);

// let it start
tremit.init();

// active sessions handler
io.on('connection', function(sock){
    tremit.clients++;
    tremit.sendStack(sock);
    sock.on('disconnect', function(){
        tremit.clients--;
        console.log('Active: ' + tremit.clients);
    });
});
