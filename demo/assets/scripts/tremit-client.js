var TremitLive = function(container){
    this.$container = container;
    this.maxRows = this.$container.data('max-lines') || 3;
    this.paymentTypes = window.paymentTypes || [];
    this.socket = io('http://payeer.liveri.ga:3040/');
    this.init();
};

TremitLive.prototype.init = function(){
    this.$container.html('');
    var _ = this;
    this.socket.on('message', function(payload){
        _.buildCell(payload);
    });
    setInterval(function(){
        _.cellManager();
    }, 1000);
};

TremitLive.prototype.cellManager = function(){
    var max = this.maxRows;

    // manage rows
    this.$container.find('tr').each(function(i, e){
        if (!$(e).hasClass('show'))
            $(e).remove();
        if (i >= max){
            $(e).removeClass('show');
        }
    });
};

TremitLive.prototype.formatMonetary = function(currency, amount){
    var cClass, cName, up = Math.floor(amount), down = amount;

    switch (currency){
        case 'USD':
            cClass = 'dollar'; cName = '$';
            down = Math.floor((amount - up) * 100).toFixed(0);
            if (down == 0) down = '00';
            break;
        case 'EUR':
            cClass = 'euro'; cName = '€';
            down = Math.floor((amount - up) * 100).toFixed(0);
            if (down == 0) down = '00';
            break;
        case 'RUR':
            cClass = 'rub'; cName = '&#8381;';
            down = '';
            break;
        case 'BTC':
            cClass = 'bit'; cName = '<s>B</s>';
            down = Math.floor((amount - up) * 10000);
            break;
    }
    return '<span class="color color--' + cClass + '"><span class="money-big"><span class="currency">' + cName + '</span>' +
        up + '</span><span class="money-small">' + down + '</span></span>';
};

TremitLive.prototype.dateFormat = {
    ru: 'DD.MM.YYYY [в] HH:mm:ss',
    en: 'MM/DD/YYYY [at] h:mm:ss a'
};

TremitLive.prototype.buildCell = function(payload){
    var time = moment(payload.timestamp).format(this.dateFormat[moment().locale()] || this.dateFormat.en); // localizable
    var html = '<td><p class="date js-momentor" data-date="' + payload.timestamp + '">' + time + '</p></td>' +
        '<td><p class="id">' + payload.uid + '</p></td>' +
        '<td><p class="info">' + (this.paymentTypes[payload.category] || '') + '</p></td>' +
        '<td><p class="sender">' + payload.from + '</p></td>' +
        '<td><p class="receive">' + payload.to + '</p></td>' +
        '<td><p class="money">' + this.formatMonetary(payload.currency, payload.amount) + '</p></td>';

    var a = this.$container.prepend($('<tr>').html(html).addClass('show'));
};


moment.locale(window.navigator.userLanguage || window.navigator.language);

new TremitLive($('.live-table .tbody'));