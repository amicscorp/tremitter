var gulp = require('gulp'),
    nodemon = require('gulp-nodemon');

gulp.task('run', function(){
    nodemon({
        script: 'app.js',
        ext: 'js'
    }).on('restart', function(){
        console.log('restarted');
    })
});

gulp.task('default', ['run']);
